//
//  ViewController.swift
//  TaskManager
//
//  Created by Subash Parajuli on 12/20/20.
//

import UIKit

class ViewController: UIViewController {
    
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.register(AssignedEmployeeTVCell.self, forCellReuseIdentifier: cellIdentifier)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.allowsSelection = false
        tableView.backgroundColor = UIColor.clear
        tableView.separatorStyle = .none        
        tableView.isUserInteractionEnabled = true
        tableView.tableFooterView = UIView()
        
        return tableView
    }()
    
    var cellIdentifier: String {
        get {
            return AssignedEmployeeTVCell.description()
        }
    }
    
    
    
     let headers = ["Assigned Employees", "Employees"]
    
     var assignedEmployees = [String]() {
        didSet {
            //assignedEmployees = ["Ram", "Hari"]
            self.tableView.reloadData()
        }
    }
     private var removedEmployees = [String]()
    private var newlyAdded = [String]()
    
    
     var employees = [String]() {
        didSet {
            self.tableView.reloadData()
        }
    }
    
//    lazy var searchView: UIView = {
//        let view = UIView()
//        view.backgroundColor = UIColor.white
//        view.translatesAutoresizingMaskIntoConstraints = false
//        view.heightAnchor.constraint(equalToConstant: 40).isActive = true
//        return view
//    }()
    
    var searchBar: UISearchBar?

    
    
    
    
    //MARK:- View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        hideKeyboardWhenTappedAround()
        
        employees = ["Ramesh", "Mahesh", "Gopal"]
        assignedEmployees = ["Ram", "Hari", "Chari", "Pari"]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupStatusBar()
        view.backgroundColor = .systemBackground
    }
    

    //MARK:- Helper Methods
    @objc func confirmAdd(){
        print("Confirmation dialog here")
        print(newlyAdded)
        print("Removed", removedEmployees)
        
        var existing = [String]()
        
        if newlyAdded.isEmpty {
            //if no items are added, all assigned employees are existing
            existing = assignedEmployees
        } else {
            //remove newly added employees from assigned list to get pre existing ones
            for item in newlyAdded {
                existing =  assignedEmployees.filter {$0 != item}
            }
            
            
        }
        
        print("Preexisting items", existing)
        
        
        //reset list
        newlyAdded = []
        removedEmployees = []
        
    }
    
    @objc func toggleEmployeeSelection(_ sender: UIButton) {
        //change the selection image of each button
        
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected {
            
            let buttonPostion = sender.convert(sender.bounds.origin, to: tableView)
            let indexPath = tableView.indexPathForRow(at: buttonPostion)
            let section = indexPath?.section
            if section == 0 && !assignedEmployees.isEmpty {
                
                let titleToRemove = assignedEmployees[indexPath!.row]
                assignedEmployees.remove(at: indexPath!.row)
                employees.append(titleToRemove)
                removedEmployees.append(titleToRemove)
                let checkImage = UIImage(named: "check")
                sender.setImage(checkImage, for: .normal)
                return
            }
            
            assign(row: indexPath!.row)
            
            
        }
    }
    
    func assign(row: Int){
        
        let titleToAdd = employees[row]
        employees.remove(at: row)
        assignedEmployees.append(titleToAdd)
        newlyAdded.append(titleToAdd)
    }
}

