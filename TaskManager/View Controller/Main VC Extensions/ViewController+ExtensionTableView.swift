//
//  ViewController+ExtensionTableView.swift
//  TaskManager
//
//  Created by Subash Parajuli on 12/20/20.
//

import Foundation
import UIKit

//MARK:- Extension tableview data source
extension ViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        ///provided that the employees assigned to a task may be 0, but not the employees in the list
        
        return assignedEmployees.isEmpty || employees.isEmpty ? 1: 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        
        case 0:
            return assignedEmployees.isEmpty ? employees.count: assignedEmployees.count
            
        case 1:
            return employees.count
            
        default:
            return 0
        }
    }
    
    func loadCells(cell: AssignedEmployeeTVCell, name: String, checkUncheck: String) {
        cell.labelEmployeeName.text = name
        let checkImage = UIImage(named: checkUncheck)
        cell.buttonSelectEmployee.setImage(checkImage, for: .normal)
        cell.buttonSelectEmployee.addTarget(self, action: #selector(toggleEmployeeSelection), for: .touchUpInside )
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: AssignedEmployeeTVCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! AssignedEmployeeTVCell
        var name = ""
        var checkUncheckImage = ""
        switch indexPath.section {
        case 0:
            
            if !assignedEmployees.isEmpty {
                name = assignedEmployees[indexPath.row]
                checkUncheckImage = "check"

                
            } else {
                
                name = employees[indexPath.row]
                checkUncheckImage = "uncheck"


            }
            
        case 1:
            
            name = employees[indexPath.row]
            checkUncheckImage = "uncheck"


            
        default:
            break
        }
        
        loadCells(cell: cell, name: name, checkUncheck: checkUncheckImage)
        return cell
        
    }
    
    
}


//MARK:- Extension tableview delegate
extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if assignedEmployees.isEmpty {
            return headers[1]
        } else if employees.isEmpty {
            return headers[0]
        } else {
            return headers[section]
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        view.tintColor = .systemBackground
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView()
        let dummyView = UIView() //just a dummy view to return
        let separatorView = UIView(frame: CGRect(x: tableView.separatorInset.left, y: footerView.frame.height, width: tableView.frame.width - tableView.separatorInset.right - tableView.separatorInset.left, height: 0.5))
        separatorView.backgroundColor = UIColor.lightGray
        footerView.addSubview(separatorView)
        
        //should be shown below the first section when the two sections are shown
        if section == 0 && !assignedEmployees.isEmpty && !employees.isEmpty {
            return footerView
        }
        return dummyView
    }
    


    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
}





