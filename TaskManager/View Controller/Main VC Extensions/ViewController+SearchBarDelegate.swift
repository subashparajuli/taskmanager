//
//  ViewController+SearchBarDelegate.swift
//  TaskManager
//
//  Created by Subash Parajuli on 12/20/20.
//

import Foundation
import UIKit

extension ViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()

    }
}
