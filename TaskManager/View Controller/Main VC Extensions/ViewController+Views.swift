//
//  ViewController+Views.swift
//  TaskManager
//
//  Created by Subash Parajuli on 12/20/20.
//

import Foundation
import UIKit

extension ViewController {
    
    func setupViews(){
        view.addSubview(tableView)
        self.setupConstraints()
        self.setupNavBar()
//        self.setupStatusBar()
        self.setupSearch()
    }
    
    private func setupNavBar() {
        
        let addButton = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 25))
        addButton.setTitle("ADD", for: .normal)
        addButton.backgroundColor = UIColor.statusBarColor
        addButton.setTitleColor(UIColor.white, for: .normal)
        addButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
        addButton.clipsToBounds = true
        addButton.addTarget(self, action: #selector(confirmAdd), for: .touchUpInside)
        
        addButton.layer.cornerRadius = 5
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: addButton)
        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.black]
        
        navigationItem.title = "Assign Employee"
        
        
        
    }
    
    func setupStatusBar() {
        
        if #available(iOS 13.0, *) {
            
            //status bar background color
            let keyWindow = UIApplication.shared.connectedScenes
                .filter({$0.activationState == .foregroundActive})
                .map({$0 as? UIWindowScene})
                .compactMap({$0})
                .first?.windows
                .filter({$0.isKeyWindow}).first
            
            let scene = keyWindow?.windowScene
            guard let windowScene = (scene) else { return }
            if let statusBarFrame = windowScene.statusBarManager?.statusBarFrame {
                let statusBarHeight = statusBarFrame.size.height
                let statusBarView = UIView.init(frame: CGRect.init(x: 0, y: -statusBarHeight, width: UIScreen.main.bounds.width, height: statusBarHeight))
                statusBarView.backgroundColor = UIColor.statusBarColor
                self.navigationController?.navigationBar.addSubview(statusBarView)
            }
            
        }
        
    }
    
    
    
    private func setupSearch() {
        
        let searchBar = UISearchBar()
        searchBar.frame = CGRect(x: 0, y: 0, width: 200, height: 70)
        searchBar.delegate = self
        searchBar.showsCancelButton = false
        searchBar.searchBarStyle = UISearchBar.Style.default
        searchBar.placeholder = " Search Employees..."
        searchBar.sizeToFit()
        tableView.tableHeaderView = searchBar
        
        
    }
    
    
    
    private func setupConstraints() {
        
        NSLayoutConstraint.activate([
 
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 8),
            tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
            
            
        ])
    }
    
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    
    @objc func dismissKeyboard() {
        view.endEditing(true)

    }
}
