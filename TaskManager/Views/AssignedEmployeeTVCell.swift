//
//  AssignedEmployeeTVCell.swift
//  TaskManager
//
//  Created by Subash Parajuli on 12/20/20.
//

import UIKit

class AssignedEmployeeTVCell: UITableViewCell {

    lazy var imageEmployee: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.clipsToBounds = true
        let radius: CGFloat = 20
        image.layer.cornerRadius = radius
        image.widthAnchor.constraint(equalToConstant: radius*2).isActive = true
        image.heightAnchor.constraint(equalToConstant: radius*2).isActive = true
        image.backgroundColor = .red
        image.image = UIImage(named: "bailey")
        
        return image

    }()

    lazy var labelEmployeeName: UILabel = {
        //set font, text color etc.
        let labelName = UILabel()
        labelName.translatesAutoresizingMaskIntoConstraints = false
        labelName.textAlignment = .left
        labelName.font = UIFont.boldSystemFont(ofSize: 16)
        return labelName

    }()

    lazy var buttonSelectEmployee: UIButton = {
       let button = UIButton()
        let side: CGFloat = 30
        button.translatesAutoresizingMaskIntoConstraints = false
        button.widthAnchor.constraint(equalToConstant: side).isActive = true
        button.heightAnchor.constraint(equalToConstant: side).isActive = true
        button.isUserInteractionEnabled = true
        return button
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(imageEmployee)
        addSubview(labelEmployeeName)
        contentView.addSubview(buttonSelectEmployee)
        self.setupViews()
        self.setupConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setupConstraints() {
        
        
        NSLayoutConstraint.activate([
            imageEmployee.leadingAnchor .constraint(equalTo: self.leadingAnchor,constant: 20),
            imageEmployee.centerYAnchor.constraint(equalTo: self.centerYAnchor),

            labelEmployeeName.leadingAnchor.constraint(equalTo: imageEmployee.trailingAnchor, constant: 8),
            labelEmployeeName.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            
            buttonSelectEmployee.leadingAnchor.constraint(equalTo: labelEmployeeName.trailingAnchor, constant: 8),
            buttonSelectEmployee.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20),
            buttonSelectEmployee.centerYAnchor.constraint(equalTo: self.centerYAnchor)
        ])
        
        
        
//        imageEmployee.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20).isActive = true
//        imageEmployee.widthAnchor.constraint(equalToConstant: 40).isActive = true
//        imageEmployee.heightAnchor.constraint(equalToConstant: 40).isActive = true
//        imageEmployee.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
//
//        labelEmployeeName.leadingAnchor.constraint(equalTo: imageEmployee.trailingAnchor, constant: 20).isActive = true
//        labelEmployeeName.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
//
//        buttonSelectEmployee.leadingAnchor.constraint(equalTo: labelEmployeeName.trailingAnchor, constant: 20).isActive = true
//        buttonSelectEmployee.widthAnchor.constraint(equalToConstant: 25).isActive = true
//        buttonSelectEmployee.heightAnchor.constraint(equalToConstant: 25).isActive = true
//        buttonSelectEmployee.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
//        buttonSelectEmployee.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20).isActive = true




    }

    func setupViews() {
        //background color, selection style etc.


    }



}


